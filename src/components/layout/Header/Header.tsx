import { Link } from 'react-router-dom';
import './Header.scss';

function Header() {
  return (
 <>
  <header className="header">
    <nav>
      <Link className="link" to="/">Inicio</Link>
      
      <Link  className="link" to="/login">Login</Link>
    </nav>
  </header>
 </>
  );
}

export default Header;
