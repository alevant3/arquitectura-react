type Props = {
    onSubmit: React.FormEventHandler
}

const TodoForm = ({ onSubmit } : Props) => {
    return (
         <form onSubmit={onSubmit}>
             <input type="text" />
             <button>Agregar</button>  
         </form>    
    );
 }
 
 export default TodoForm;