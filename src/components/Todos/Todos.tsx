import * as React from 'react';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
// actions
import { addTodo, updateTodo, deleteTodo } from '../../redux/actions/todoActions';
import { getId } from '../../utils/utils';
import TodoForm from "./TodoForm";
import { ITodo } from './../../types/types';

// hooks
import { useLocalStorage } from './../hooks/useLocalStorage';


const Todos = () => {
  // utilizar use dispatch y te crea una función
  const dispatch = useDispatch();

  // Acceder al state del store
  const todo = useSelector( (state: RootStateOrAny) => state.todo );

  const [task, setTask] = useLocalStorage('task', ''); 

  const handleSubmit = (e: React.BaseSyntheticEvent) => {
    e.preventDefault();
    const text =  e.target[0].value;
    console.log(text)
    console.log(e)
    dispatch(addTodo({
      text,
      checked: false,
      id: getId()
    }));
    (e.currentTarget as HTMLInputElement).value = '';
  }
  return (
    <div>
      <TodoForm onSubmit={handleSubmit} />
      <ul>
        {todo.todos.map((todo: ITodo, index: number) => (
            <li key={todo.id}onClick={()=> {dispatch(updateTodo(todo))}}>
              {" "}
              <input type="checkbox"  checked={todo.checked}  />
              <button onClick={()=>dispatch(deleteTodo(todo))}>x</button>
              {todo.text}
            </li>
        ))}
      </ul>
    </div>
  );
};


export default Todos;
