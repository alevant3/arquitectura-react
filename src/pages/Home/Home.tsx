
import Todos from '../../components/Todos';

function Home() {
  return (
    <div>
       <h1>Todo List</h1>
       <Todos/>
    </div>
  );
}

export default Home;
