import { Provider } from 'react-redux';
import  store from './redux/store';
import RootRouter from './routes/Root.routes';

function App() {
  return (
    <Provider store={store}>
        <RootRouter/>
   </Provider>
  );
}

export default App;
