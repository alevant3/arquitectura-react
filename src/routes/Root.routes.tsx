import {
    BrowserRouter as Router,
    Route,
    Switch,
  } from "react-router-dom";

// Layout
import Header from '../components/layout/Header/Header';
import Footer from '../components/layout/Footer/Footer';

// Components
import  Home  from '../pages/Home/Home';
import  Login  from '../pages/Login/Login';
// paths
import { PATHS } from './paths';

const RootRouter = () => {
  return (
      <Router>
        <Header/>
         <Switch>
            <Route exact path={PATHS.HOME} component={Home} />
            <Route exact path={PATHS.LOGIN} component={Login} />
          </Switch>
          <Footer/>
      </Router>
  );
}

export default RootRouter;