import { ITodo } from "./../../types/types";
export const ADD_TODO = "ADD_TODO";
export const UPDATE_TODO = "UPDATE_TODO ";
export const DELETE_TODO = "DELETE_TODO";

export const addTodo = (todo: ITodo) => {
  return {
    type: ADD_TODO,
    payload: todo,
  };
};

export const updateTodo = (todo: ITodo) => {
  return {
    type: UPDATE_TODO,
    payload: todo,
  };
};

export const deleteTodo = (todo: ITodo) => {
  return {
    type: DELETE_TODO,
    payload: todo,
  };
};
