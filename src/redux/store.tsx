import { createStore, compose, applyMiddleware } from "redux";
import {rootReducer} from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__?: typeof compose;
  }
}

// middleware
const logger = (store: any) => (next: Function) => (action: {type: string, payload: any}) => {
  console.log('Ha ocurrido una nueva acción', action);
  next(action);
}


const store = createStore(rootReducer, 
  compose( 
  applyMiddleware(logger),
  typeof window === 'object' &&
      typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined' ? 
          window.__REDUX_DEVTOOLS_EXTENSION__() : (f: Function) => f
)
  );

export default store;
