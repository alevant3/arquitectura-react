import { ADD_TODO, UPDATE_TODO, DELETE_TODO } from "./../actions/todoActions";

import { ITodo } from "./../../types/types";

const initialState = {
  todos: [
    {
      text: "Tarea 1",
      id: "1",
      checked: true,
    }
  ],
};

type Action = {
  type: string,
  payload: ITodo
}

function todo(state = initialState, action: Action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };

    case UPDATE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) => {
          if (action.payload.id === todo.id) {
            return {
              ...todo,
              checked: !todo.checked,
            };
          }
          return todo;
        }),
      };
    case DELETE_TODO:
        return {
            ...state,
            todos: state.todos.filter((todo) => {
              return todo.id !== action.payload.id
            })
          }
    
    default:
      return state;
  }
}

export default todo;
